# SlackSlurp API

Provides a programmatic interface to use the SlackSlurp API to store and retrieve channels, messages, users of a "slurped" Slack workspace.

## Installation

This project was developed and testing using Go 1.17

```
go get gitlab.com/0x192/slackslurpapi
```

## Basic Usage

### Initialize Client

You will need to request an API Token from the author of this project (0x192)

```Go
client := slackslurpapi.NewClient("<api_key>")
```

### Get Item by ID

```Go
client.GetChannel("<channel_id>")
client.GetMessage("<message_id>")
client.GetUser("<user_id>")
```