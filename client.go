package slackslurpapi

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	slurpApiBaseURL = "https://api.slackslurp.com"
)

// Client client for slurp api
type Client struct {
	client  *http.Client
	baseURL string
	apiKey  string
}

// NewClient slurp api client constructor
func NewClient(apiKey string) *Client {
	return &Client{
		client:  http.DefaultClient,
		apiKey:  apiKey,
		baseURL: slurpApiBaseURL,
	}
}

// GET functions
// TODO: Add MessageList function w/ pagination
func (c *Client) GetChannel(objId string) (*Channel, error) {
	req, err := c.get("channels", objId, url.Values{})
	if err != nil {
		return nil, err
	}

	channel := new(Channel)
	err = c.do(req, &channel)

	return channel, err
}

func (c *Client) GetMessage(objId string) (*Message, error) {
	req, err := c.get("messages", objId, url.Values{})
	if err != nil {
		return nil, err
	}

	message := new(Message)
	err = c.do(req, &message)

	return message, err
}

// POST functions
// TODO: Add NewUser function
func (c *Client) NewChannel(channelJSON string) (*Channel, error) {
	req, err := c.post("channels", channelJSON)
	if err != nil {
		return nil, err
	}

	channel := new(Channel)
	err = c.do(req, &channel)

	return channel, err
}

func (c *Client) NewMessage(msgJSON string) (*Message, error) {
	req, err := c.post("messages", msgJSON)
	if err != nil {
		return nil, err
	}

	message := new(Message)
	err = c.do(req, &message)

	return message, err
}

// HTTP request contructors per HTTP method
func (c *Client) post(objType, data string) (*http.Request, error) {
	uri := fmt.Sprintf("%s/%s", c.baseURL, objType)

	body := []byte(data)
	req, err := http.NewRequest("POST", uri, bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("cannot create request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("AuthToken", c.apiKey)

	return req, nil
}

func (c *Client) get(objType, objId string, urlParams url.Values) (*http.Request, error) {
	uri := fmt.Sprintf("%s/%s", c.baseURL, objType)
	if objId != "" {
		uri += fmt.Sprintf("/%s", objId)
		urlParams = url.Values{}
	}

	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot create request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("AuthToken", c.apiKey)
	req.URL.RawQuery = urlParams.Encode()

	return req, nil
}

func (c *Client) do(req *http.Request, response interface{}) error {
	if req == nil {
		return errors.New("nil request")
	}

	uri := req.URL.RequestURI()

	resp, err := c.client.Do(req)
	if err != nil {
		return fmt.Errorf("HTTP request failure on %s: %w", uri, err)
	}

	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return makeHTTPClientError(uri, resp)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("HTTP Read error on response for %s: %w", uri, err)
	}

	err = json.Unmarshal(b, response)
	if err != nil {
		return fmt.Errorf("JSON decode failed on %s:\n%s\nerror:%w", uri, string(b), err)
	}

	return nil
}
