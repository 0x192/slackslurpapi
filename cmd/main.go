package main

import (
	"fmt"
	"os"

	"gitlab.com/0x192/slackslurpapi"
)

func main() {
	apiKey := os.Args[1]
	if apiKey == "" {
		fmt.Println("no api key supplied")
		os.Exit(1)
	}

	client := slackslurpapi.NewClient(apiKey)
	msg, err := client.GetMessage(os.Args[2])
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(msg.Text)
}
