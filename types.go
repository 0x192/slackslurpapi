package slackslurpapi

type Channel struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	IsChannel  bool   `json:"is_channel"`
	IsArchived bool   `json:"is_archived"`
	IsIM       bool   `json:"is_im"`
}

type Reaction struct {
	Name  string   `json:"name"`
	Users []string `json:"users"`
	Count uint     `json:"count"`
}

type Message struct {
	Id          string   `json:"id"`
	Type        string   `json:"type"`
	SubType     string   `json:"subtype"`
	Text        string   `json:"text"`
	User        string   `json:"user"`
	Ts          string   `json:"ts"`
	Team        string   `json:"team"`
	ThreadTs    string   `json:"thread_ts"`
	ReplyCount  uint     `json:"reply_count"`
	ReplyUsersC uint     `json:"reply_users_count"`
	LatestReply string   `json:"latest_reply"`
	ReplyUsers  []string `json:"reply_users"`
	IsLocked    bool     `json:"is_locked"`
	Edited      struct {
		User string `json:"user"`
		Ts   string `json:"ts"`
	} `json:"edited"`
	Reactions []Reaction `json:"reactions"`
}
